---
layout: markdown_page
title: "Marketing"
---

The marketing team is made up of a few key functional groups:

## Design

- [Designer](/jobs/designer/)

## Demand generation

- [Demand generation](/handbook/marketing/demand-generation)
- [Online marketing](/handbook/marketing/online-marketing/)
- [Business development](/jobs/business-development-representative/)

## Developer Relations

- [Technical writing](/jobs/technical-writer/)
- [Developer advocacy](/handbook/marketing/developer-relations/developer-advocacy/)
- [Field marketing](/handbook/marketing/developer-relations/field-marketing/)
- [Content marketing](/handbook/marketing/developer-relations/content-marketing/)

## Product Marketing

- [Product marketing](/handbook/marketing/product-marketing/)
- [Partner marketing](/handbook/marketing/product-marketing/#partnermarketing/)

## Marketing resources

- [GitLab Marketing Project](https://gitlab.com/gitlab-com/marketing)
- Google drive folder can be found by searching the company Google Drive for
  "GitLab Marketing"

[Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/issues)
