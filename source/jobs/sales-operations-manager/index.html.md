---
layout: markdown_page
title: "Sales Operations Manager"
---


GitLab is hiring a Sales Ops Manager to help us scale. Your mission is to organize data and generate deep customer insight in order to enhance sales force productivity and effectiveness.
You will accomplish your mission by working collaboratively with our sales, marketing, finance and operations teams, you will measure everything to help us optimize our sales funnel and achieve growth.
Your impact will help us make it easier to maintain a competitive edge, enhance sales force effectiveness, and achieve consistent, sustainable sales success.

## Responsibilities

* Responsible for implementing tools and processes for the sales organization that focus on improving efficiency and effectiveness
* Manager Platforms and Systems: Responsible for all SalesForce.com administration including user management, data management, application setup, customization, reports and dashboards. Zuora, WebEx, TrainTool, etc.
* Partner in the development of sales coverage, headcount planning and sales quota.
* Create and manage New Hire Boot Camps to quickly ramp up Sales Reps
* Improve and manage the [Sales Onboarding Bootcamp](https://about.gitlab.com/handbook/sales-onboarding/) 
* Create Board level presentations for the Chief Revenue Officer
* Manage, analyze and summarize the weekly Sales Forecast
* Create and manage programs for Weekly and Quarterly Sales Training sessions
* Organize and manage the sales resource library to include marketing pieces, presentations, outreach campaigns and competitive market intelligence.
* Pricing and Contract Support: Given the pace of business, its imperative that sales operations enable the sales team with high-quality proposals that can be turned around quickly and efficiently.


## Requirements for Applicants
(Check our [Jobs](https://about.gitlab.com/jobs/) page to see current openings).

* Minimum 5 years Sales Ops experience required
* Strong analytical ability and able to prioritize multiple projects
* Strong communication skills
* Effective multi-tasking and time management required
* Deadline driven
* SaaS and B2B experience preferred
* Interest in GitLab, and open source software

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto]( https://about.gitlab.com/2015/04/08/the-remote-manifesto/)!
